It is necessary to solve the problem of forming a list of recommended
products for an online store user using a cross-
correlation algorithm (having a set of tuples of objects, for each possible pair
of objects, count the number of tuples where they meet together).
1. Implement two algorithms on MapReduce:
- Cross Correlation Pairs.
- Cross Correlation Stripes.
2. Write an online order database generator (or take a ready-made one).
Take into account that the order consists of an arbitrary number of items (goods).
3. Use the cross-correlation algorithm to process the order database
(count the number of occurrences of each pair of products).
4. Implement the Expert Advisor component without applying the MapReduce pattern.
Enter the product name. The 10 products that are most often
bought with the specified product are displayed. Reading the results of the algorithm
cross-correlations from HDFS are implemented manually (for Java-using
FileSystem API).
