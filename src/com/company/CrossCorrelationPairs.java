package com.company;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class CrossCorrelationPairs {
    public CrossCorrelationPairs(Configuration configuration, Path pathDirInput) throws IOException, InterruptedException, ClassNotFoundException {
        Job job = Job.getInstance(configuration, "purchase history");
        job.setJarByClass(Main.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, pathDirInput);
        FileOutputFormat.setOutputPath(job, new Path("GroceryStore/output_cross_correlation_pairs"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
    public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String[] parserStr = value.toString().split(" ");
            for (int i = 0, count = 0; i < parserStr.length; ++i) {
                for (int j = 0; j < parserStr.length; ++j, ++count)
                    if(j != i) context.write(new Text(parserStr[i] + " " + parserStr[j]), one);
            }
        }
    }
    public static class IntSumReducer extends Reducer< WritableComparable<?> , IntWritable, WritableComparable<?>, IntWritable> {
        private final IntWritable result = new IntWritable();
        public void reduce(WritableComparable<?> key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            result.set(sum);
            context.write(key, result);
        }
    }
}
