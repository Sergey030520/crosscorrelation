package com.company;

import javafx.util.Pair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class SmartAdvisor {
    private File dataFile;
    private final HashMap<String, ArrayList<Pair<String, Integer>>> data;
    public SmartAdvisor(Path inPathDownloadDir, Configuration configuration) throws IOException {
        HdfsClient hdfsClient = new HdfsClient(configuration);
        data = new HashMap<>();
        try{
            File pathWorkingDir = new File("src/com/company/working_directory");
            if(pathWorkingDir.exists()) {
                if (!pathWorkingDir.delete()) System.out.println("Unable to delete the file!");
            }
            if(!pathWorkingDir.mkdirs()) System.out.println("Failed to create a directory!");
            hdfsClient.get(inPathDownloadDir, new Path(pathWorkingDir.toString()));
            dataFile = new File((pathWorkingDir.toPath().toString().charAt(pathWorkingDir.toPath().toString().length()-1) != '/' ?
                    pathWorkingDir.toPath() + "/part-r-00000": pathWorkingDir.toPath() + "part-r-00000"));
            loadData();
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
    private void loadData(){
        try {
            FileReader fileReader = new FileReader(dataFile);
            StringBuilder line = new StringBuilder();
            for (int symbol, count = 0; (symbol = fileReader.read()) != -1; ) {
                if ((char) symbol != '\n') {
                    line.append((char) symbol);
                } else {
                    String[] newArrayParse = line.toString().replace(":", "").replace((char) 9, ' ').split(" +");
                    if(data.get(newArrayParse[0]) != null){
                        data.get(newArrayParse[0]).add(new Pair<>(newArrayParse[1], Integer.parseInt(newArrayParse[2])));
                    }else{
                        data.put(newArrayParse[0], new ArrayList<>());
                        data.get(newArrayParse[0]).add(new Pair<>(newArrayParse[1], Integer.parseInt(newArrayParse[2])));
                    }
                    line = new StringBuilder();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
    public void play(){
        String nameProduct;
        do{
            System.out.println("Input the name product(exit): ");
            Scanner scanner = new Scanner(System.in);
            nameProduct = scanner.next();
            if(!Objects.equals(nameProduct, "exit")) ShowRes(nameProduct, find(nameProduct));
        }while(!nameProduct.equals("exit"));
    }
    private ArrayList<Pair<String, Integer>> find(String nameProduct){
        ArrayList<Pair<String, Integer>> products = data.get(nameProduct);
        if(products != null){
            for (int i = 0; i < products.size()-1; i++) {
                for (int j = i; j < products.size(); j++) {
                    if(i != j && products.get(i).getValue() < products.get(j).getValue()){
                        Pair<String, Integer> pair = products.get(i);
                        products.set(i, products.get(j));
                        products.set(j, pair);
                    }
                }
            }
        }
        return products;
    }
    private void ShowRes(String nameProduct, ArrayList<Pair<String, Integer>> resListProducts){
        if(resListProducts != null) {
            System.out.println("With this product, you usually buy:");
            for (int i = 0; i < resListProducts.size() && i < 10; i++) {
                System.out.println("\tName product: " + resListProducts.get(i).getKey() +
                        " (The number of people who chose this product with " + nameProduct +": "
                        + resListProducts.get(i).getValue() + ")");
            }
        }else{
            System.out.println("This product is not in the store!");
        }
    }
}
