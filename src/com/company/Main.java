package com.company;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Progressable;
import org.apache.hadoop.yarn.webapp.hamlet2.Hamlet;
import org.eclipse.jetty.util.ajax.JSON;
import org.iq80.leveldb.DB;

import java.io.*;
import java.net.URI;
import java.nio.file.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Main {
    private static final Path pathGroceryStore = new Path("/user/sergeimakarov/GroceryStore");
    private static final Path pathDirInput = new Path("/user/sergeimakarov/GroceryStore/input/");
    private static final Path pathDirOutputStripes = new Path("/user/sergeimakarov/GroceryStore/output_cross_correlation_stripes/");
    private static final Path pathDirOutputPairs = new Path("/user/sergeimakarov/GroceryStore/output_cross_correlation_pairs/");
    private static final Configuration configuration = new Configuration();
    private static  HdfsClient hdfsClient;
    public static void loadFile(){
        try {
            if (!hdfsClient.existHdfs(pathGroceryStore)) hdfsClient.mkdirsHdfs(pathGroceryStore);
            hdfsClient.deleteHdfs(pathDirInput);
            hdfsClient.deleteHdfs(pathDirOutputStripes);
            hdfsClient.deleteHdfs(pathDirOutputPairs);
            hdfsClient.mkdirsHdfs(pathDirInput);
            hdfsClient.put(new Path("src/com/company/PurchaseHistory"), pathDirInput);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        configuration.set("fs.defaultFS", "hdfs://0.0.0.0:9000/");
        hdfsClient = new HdfsClient(configuration);
        GenerateBD(new File("/home/sergeimakarov/IdeaProjects/CrossCorrelation/src/com/company/PurchaseHistory"),
                new File("/home/sergeimakarov/IdeaProjects/CrossCorrelation/src/com/company/Products"));
        loadFile();
        CrossCorrelationPairs crossCorrelationPairs = new CrossCorrelationPairs(configuration, pathDirInput);
        CrossCorrelationStripes crossCorrelationStripes = new CrossCorrelationStripes(configuration, pathDirInput);
        SmartAdvisor smartAdvisor = new SmartAdvisor(new Path(pathDirOutputStripes+"/part-r-00000"), configuration);
        /*SmartAdvisor smartAdvisor = new SmartAdvisor(new Path(pathDirOutputPairs+"/part-r-00000"), configuration);*/
        smartAdvisor.play();
    }
    public static void  GenerateBD(File bd, File products){
        try {
            FileReader fileReader = new FileReader(products);
            ArrayList<String> productsList = new ArrayList<>();
            productsList.add("");
            for(int count = 0, symbol;(symbol = fileReader.read())!=-1; ){
                if((char)symbol == '\n'){
                    productsList.add("");
                    ++count;
                }
                else productsList.set(count, productsList.get(count)+(char)symbol);
            }
            try{
                FileWriter fileWriter = new FileWriter(bd, false);
                int countClients = (int)getRandomIntegerBetweenRange(5,  50);
                for (int numbClients = 0; numbClients < countClients; ++numbClients){
                    int countProduct = (int)getRandomIntegerBetweenRange(3, 5);
                    StringBuilder list_product = new StringBuilder();
                   ArrayList<Integer> code_products = new ArrayList<>();
                    for (int numbProduct = 0; numbProduct < countProduct; ++numbProduct){
                        int idProducts = (int)getRandomIntegerBetweenRange(0, productsList.size()-1);
                        boolean isMissing = true;
                        if(numbProduct != 0) {
                            for (Integer code_product : code_products) {
                                if (code_product == idProducts) {
                                    --numbProduct;
                                    isMissing = false;
                                    break;
                                }
                            }
                        }
                        if(isMissing || (numbProduct == 0)){
                            list_product.append(productsList.get(idProducts)).append(" ");
                            code_products.add(idProducts);
                        }
                    }
                    if(numbClients != countClients - 1) list_product.append(" \n");
                    fileWriter.write(list_product.toString());
                }
                fileWriter.flush();
                fileWriter.close();
            }catch(IOException ioException){
                System.out.println(ioException.getMessage());
            }
            fileReader.close();
        }catch (IOException ioException){
            System.out.println(ioException.getMessage());
        }
    }
    public static double getRandomIntegerBetweenRange(double min, double max){
        return (int)(Math.random()*((max-min)+1))+min;
    }

}
