package com.company;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CrossCorrelationStripes {
    public CrossCorrelationStripes(Configuration configuration, Path pathDirInput) throws IOException, InterruptedException, ClassNotFoundException {
        Job job = Job.getInstance(configuration, "purchase history");
        job.setJarByClass(Main.class);
        job.setMapperClass(CrossCorrelationStripes.TokenizerMapper.class);
        job.setReducerClass(CrossCorrelationStripes.IntSumReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(AssociativeArrayMap.class);
        job.setOutputValueClass(AssociativeArrayMap.class);
        FileInputFormat.addInputPath(job, pathDirInput);
        FileOutputFormat.setOutputPath(job, new Path("GroceryStore/output_cross_correlation_stripes"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
    public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String[] parserStr = value.toString().split(" ");
            for (int i = 0, count = 0; i < parserStr.length; ++i) {
                HashMap<String, Integer> hashMap = new HashMap<>();
                for (int j = 0; j < parserStr.length; ++j, ++count){
                       if(i != j) hashMap.put(parserStr[j], 1);
                }
                context.write(new Text(parserStr[i]), new AssociativeArrayMap(hashMap));
            }
        }
    }
    public static class IntSumReducer extends Reducer<WritableComparable<?>, AssociativeArrayMap, WritableComparable<?>, IntWritable> {
        private final IntWritable result = new IntWritable();
        public void reduce(WritableComparable<?> key, Iterable<AssociativeArrayMap> values, Context context) throws IOException, InterruptedException {
            AssociativeArrayMap associativeArrayMap = new AssociativeArrayMap(new HashMap<>());
            for(AssociativeArrayMap val : values){
                associativeArrayMap.SumMerge(val.getHashMap());
            }
            HashMap<String, Integer> resHashMap = associativeArrayMap.getHashMap();
            for (Map.Entry<String, Integer> entry: resHashMap.entrySet()){
                result.set(entry.getValue());
                context.write(new Text(key.toString() + " : " + entry.getKey()), result);
            }
        }
    }

}
