package com.company;

import com.google.common.base.Splitter;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AssociativeArrayMap extends IntWritable implements Writable {
    private HashMap<String, Integer> hashMap;
    private String text;

    public AssociativeArrayMap(){
    }
    public AssociativeArrayMap(HashMap<String, Integer> inHashMap){
        hashMap = inHashMap;
        text = hashMap.toString();
    }
    public void SumMerge(HashMap<String, Integer> anotherHashMap){
        anotherHashMap.forEach((key, value) -> {
            final Integer merge = hashMap.merge(key, value, Integer::sum);
        });
    }
    public HashMap<String, Integer> getHashMap(){
        return hashMap;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(text);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        text = dataInput.readUTF();
        text = text.replace('{', ' ').replace('}', ' ');
        String[] textArray = text.split(",");
        hashMap = new HashMap<>();
        for (String s : textArray) {
            String[] newWords = s.split("=");
            hashMap.put(newWords[0].trim(), Integer.parseInt(newWords[1].trim()));
        }
    }
}
