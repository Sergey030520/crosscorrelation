package com.company;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.ChecksumFileSystem;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class HdfsClient {
    private  FileSystem fileSystem;
    public HdfsClient(Configuration configuration) throws IOException {
        fileSystem = FileSystem.get(configuration);
    }
    public  boolean put(Path fromPath, Path toPath) {
        try {
            fileSystem.copyFromLocalFile(fromPath, toPath);
            System.out.println("The file was uploaded successfully!");
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return true;
    }
    public  boolean deleteHdfs(Path path) throws IOException {
        if (fileSystem.exists(path)) {
            try {
                fileSystem.delete(path, true);
            } catch (IOException e) {
                System.err.println(e.getMessage());
                return false;
            }
        }
        return true;
    }
    public boolean existHdfs(Path path) throws IOException {
        return fileSystem.exists(path);
    }
    public boolean mkdirsHdfs(Path path) {
        try {
            fileSystem.mkdirs(path);
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        }
        return true;
    }
    public boolean get(Path fromPath, Path toPath){
        try{
            fileSystem.copyToLocalFile(fromPath, toPath);
            System.out.println("The file was downloaded successfully!");
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        }
        return true;
    }
}
